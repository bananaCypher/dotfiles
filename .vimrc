set nocompatible              " be iMproved, required
filetype off                  " required

" vundle install:-
" git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'SirVer/ultisnips'
    let g:UltiSnipsExpandTrigger="<tab>"
    let g:UltiSnipsJumpForwardTrigger="<tab>"
    let g:UltiSnipsJumpBackwardTrigger="<c-z>"
Plugin 'honza/vim-snippets'
Plugin 'scrooloose/nerdtree'
Plugin 'Xuyuanp/nerdtree-git-plugin'

call vundle#end()         

" activates filetype detection
filetype plugin indent on
" activates syntax highlighting among other things
syntax on
au BufNewFile,BufRead *.ejs set filetype=html
" allows you to deal with multiple unsaved
" buffers simultaneously without resorting
" to misusing tabs
set hidden
" just hit bakspace without this one and
" see for yourself
set backspace=indent,eol,start

let g:ruby_path = ":/Users/User/.rbenv/shims/ruby"

set tabstop=2       " The width of a TAB is set to 4.
set shiftwidth=2    " Indents will have a width of 4                   
set softtabstop=2   " Sets the number of columns for a TAB
set expandtab       " Expand TABs to spaces
set cursorline
hi CursorLine term=bold cterm=bold 
set relativenumber nu rnu
command W w
command Q q

execute pathogen#infect()

let mapleader = ","
nmap <leader>r :!ruby %<cr>
nmap <leader>n :NERDTree<cr>
nmap <leader>cs :vsplit ~/Documents/CodeClan/notes/cheatsheet.md<cr>
nmap <leader>js :!node %<cr>
