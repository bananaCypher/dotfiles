#
# ~/.bashrc
#

# Sources
#source /usr/share/git/completion/git-prompt.sh
source ~/.git-completion.sh
export PATH="${PATH}:/home/choggs/.gem/ruby/2.3.0/bin"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'

set_prompt () {
    Last_Command=$?
    Blue='\[\e[01;34m\]'
    White='\[\e[01;37m\]'
    Red='\[\e[01;31m\]'
    Green='\[\e[01;32m\]'
    Reset='\[\e[00m\]'
    FancyX='\342\234\227'
    Checkmark='\342\234\223'

    PS1=""
    # If it was successful, print a green check mark. Otherwise, print a red X.
    if [[ $Last_Command == 0 ]]; then
        PS1+=" $Green$Checkmark "
    else
        PS1+=" $Red$FancyX $Red\$? " 
    fi

    # If root, just print the host in red. Otherwise, print the current user and host in green.
    if [[ $EUID == 0 ]]; then
        PS1+="$Red\\h "
    else
        PS1+="$Green\\u@\\h "
    fi

    # Print the working directory and prompt marker in blue, and reset the text color to the default.
    GIT_PS1_SHOWDIRTYSTATE=true
    GIT_PS1_SHOWUNTRACKEDFILES=true
    PS1+="$Blue\\W$(__git_ps1 "$Green(%s)")\\\$$Reset "
}
PROMPT_COMMAND='set_prompt'

# Alias
alias l='ls -a'
alias ll='ls -la'
alias rmkdir='mkdir -pv'
alias svim='sudo vim'
alias edit='vim'
alias tar='tar -xvf'
