# Vimrc changes
* [x] line highlighting
* [x] relative number with 0 line having actual line number
* [x] remap :W to :w
* [x] ajax snippet
* [/] see if I can have a seperate dotfolder for custom snippets
* [x] JS create element
* [x] JS for of loop
* [x] For Of loop Snippet to make var
* [x] JS snippet for function with .bind(this); possibly funcbind
