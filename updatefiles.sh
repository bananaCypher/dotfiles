#!/bin/bash
Files=$(find ./ -depth -type f -name "*" -print | grep -v .git/ | grep -v updatefiles.sh)

for file in $Files
do
    new_file=$(echo $file | sed s/.//1)
    new_path=$(echo $new_file | rev | sed 's/[^\/]*//' | rev)
    echo "mkdir -p $HOME$new_path"
    $(mkdir -p $HOME$new_path)
    echo "cp $file $HOME$new_file"
    $(cp $file $HOME$new_file)
done
